from django.db import models

class RumahSakit(models.Model):
    name = models.CharField(max_length=100)
    province = models.CharField(max_length=50)
    address = models.TextField()
    telp = models.CharField(max_length=50)

    def __str__(self):
        return self.name