from django.apps import AppConfig


class SitusConfig(AppConfig):
    name = 'situs'
