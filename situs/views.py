from django.shortcuts import render
from situs.forms import Input_Form
from situs.models import Pesan
from django.http import HttpResponse, HttpResponseRedirect

def situs(request):
    pesans = Pesan.objects.all()
    response = {'input_form' : Input_Form(), 'pesans' : pesans}
    return render(request, 'situs.html', response)

def pesan(request):
    form = Input_Form(request.POST or None)
    if (request.method == 'POST') :
        if (form.is_valid()) :
            form.save()
            return HttpResponseRedirect('/situs')
    else :
        return HttpResponseRedirect('/')


