from django.urls import path

from . import views

app_name = 'situs'

urlpatterns = [
    path('', views.situs, name='situs'),
    path('savepesan/', views.pesan, name='pesan'),
]