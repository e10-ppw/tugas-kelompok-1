from django.urls import path
from . import views

app_name = 'beranda'

urlpatterns = [
    path('', views.home, name='home'),
    path('daftar-kritikan/', views.daftar, name='daftar'),
]