from django.db import models

class KritikSaran(models.Model):
    nama = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    kritik_saran = models.TextField(max_length=1000)